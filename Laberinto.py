import tkinter as tk
import random
from heapq import heappop, heappush
from tkinter import messagebox, simpledialog

# Dimensiones de la cuadrícula
GRID_SIZE = 40
CELL_SIZE = 15
WINDOW_WIDTH = GRID_SIZE * CELL_SIZE + 5  
WINDOW_HEIGHT = GRID_SIZE * CELL_SIZE + 45  

# Colores según el estado de la celda
COLOR_MAP = {
    0: "white",  # Celda vacía
    1: "black",  # Pared
    2: "green",  # Inicio
    3: "red",    # Fin
}

# Matriz inicial
matrix = [
    [0 for _ in range(GRID_SIZE)] for _ in range(GRID_SIZE)
]

# Posición inicial y final
start = (1, 1)
end = (GRID_SIZE-2, GRID_SIZE-2)
time_left = 0
timer_id = None
game_started = False

def create_grid(canvas, matrix):
    for i in range(GRID_SIZE):
        for j in range(GRID_SIZE):
            x1, y1 = j * CELL_SIZE, i * CELL_SIZE
            x2, y2 = x1 + CELL_SIZE, y1 + CELL_SIZE
            color = COLOR_MAP[matrix[i][j]]
            canvas.create_rectangle(x1, y1, x2, y2, fill=color, outline="black")

def update_grid(canvas, matrix):
    canvas.delete("all")
    create_grid(canvas, matrix)

def generate_maze(matrix):
    # Rellena la matriz con paredes
    for i in range(GRID_SIZE):
        for j in range(GRID_SIZE):
            matrix[i][j] = 1

    # Implementación del algoritmo de Prim para generar laberintos
    def add_walls(walls, x, y):
        if x > 1: walls.append((x-2, y))
        if x < GRID_SIZE-2: walls.append((x+2, y))
        if y > 1: walls.append((x, y-2))
        if y < GRID_SIZE-2: walls.append((x, y+2))

    walls = []
    start_x, start_y = 1, 1
    matrix[start_x][start_y] = 0
    add_walls(walls, start_x, start_y)

    while walls:
        x, y = random.choice(walls)
        walls.remove((x, y))
        if matrix[x][y] == 1:
            neighbours = []
            if x > 1 and matrix[x-2][y] == 0: neighbours.append((x-2, y))
            if x < GRID_SIZE-2 and matrix[x+2][y] == 0: neighbours.append((x+2, y))
            if y > 1 and matrix[x][y-2] == 0: neighbours.append((x, y-2))
            if y < GRID_SIZE-2 and matrix[x][y+2] == 0: neighbours.append((x, y+2))
            if neighbours:
                nx, ny = random.choice(neighbours)
                matrix[x][y] = 0
                matrix[(x+nx)//2][(y+ny)//2] = 0
                add_walls(walls, x, y)

    # Establecer los puntos de inicio y fin
    start = (1, 1)
    end = (GRID_SIZE-2, GRID_SIZE-2)
    matrix[start[0]][start[1]] = 2
    matrix[end[0]][end[1]] = 3
    return start, end

def astar_search(matrix, start, end):
    def heuristic(a, b):
        return abs(a[0] - b[0]) + abs(a[1] - b[1])

    open_set = []
    heappush(open_set, (0, start))
    came_from = {}
    g_score = {start: 0}
    f_score = {start: heuristic(start, end)}

    while open_set:
        _, current = heappop(open_set)

        if current == end:
            return True

        for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            neighbor = (current[0] + dx, current[1] + dy)
            if 0 <= neighbor[0] < GRID_SIZE and 0 <= neighbor[1] < GRID_SIZE:
                if matrix[neighbor[0]][neighbor[1]] != 1:
                    tentative_g_score = g_score[current] + 1
                    if tentative_g_score < g_score.get(neighbor, float('inf')):
                        came_from[neighbor] = current
                        g_score[neighbor] = tentative_g_score
                        f_score[neighbor] = tentative_g_score + heuristic(neighbor, end)
                        heappush(open_set, (f_score[neighbor], neighbor))

    return False

def generate_map():
    global matrix, start, end, time_left, timer_id, game_started

    try:
        time_left = int(simpledialog.askstring("Tiempo", "Ingresa el tiempo en segundos (10-120):"))
        if time_left < 10 or time_left > 120:
            messagebox.showerror("Error", "El tiempo debe estar entre 10 y 120 segundos.")
            return
    except (TypeError, ValueError):
        messagebox.showerror("Error", "Debes ingresar un número válido.")
        return

    timer_label.config(text=f"Tiempo restante: {time_left // 60}:{time_left % 60:02d}")

    if timer_id:
        canvas.after_cancel(timer_id)
        timer_id = None

    matrix = [[0 for _ in range(GRID_SIZE)] for _ in range(GRID_SIZE)]

    while True:
        start, end = generate_maze(matrix)
        if astar_search(matrix, start, end):
            break
    update_grid(canvas, matrix)
    game_started = True
    start_timer()

def start_timer():
    global time_left, timer_id, game_started
    if time_left > 0:
        time_left -= 1
        timer_label.config(text=f"Tiempo restante: {time_left // 60}:{time_left % 60:02d}")
        timer_id = canvas.after(1000, start_timer)
    else:
        game_started = False
        end_game(False)

def end_game(won):
    global timer_id, game_started
    game_started = False
    if timer_id:
        canvas.after_cancel(timer_id)
        timer_id = None
    if won:
        messagebox.showinfo("¡Felicidades!", "Has llegado a la meta.")
        ask_play_again()
    else:
        show_resolve_option()

def show_resolve_option():
    resolve = messagebox.askyesno("Perdiste", "Se acabó el tiempo. ¿Quieres ver cómo se resuelve el laberinto?")
    if resolve:
        resolve_maze()
        messagebox.showinfo("Resolución completada", "Así se resolvía el laberinto.")
    ask_play_again()

def ask_play_again():
    play_again = messagebox.askyesno("Juego Terminado", "¿Quieres jugar de nuevo?")
    if play_again:
        generate_map()
    else:
        messagebox.showinfo("Adiós", "Gracias por jugar.")
        return_to_menu()

def solve_maze(matrix, start, end):
    stack = [(start, [start])]
    visited = set()

    def heuristic(a, b):
        return abs(a[0] - b[0]) + abs(a[1] - b[1])

    while stack:
        (current, path) = stack.pop()
        if current in visited:
            continue

        visited.add(current)
        if current == end:
            return path

        neighbors = []
        for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            neighbor = (current[0] + dx, current[1] + dy)
            if 0 <= neighbor[0] < GRID_SIZE and 0 <= neighbor[1] < GRID_SIZE:
                if matrix[neighbor[0]][neighbor[1]] != 1 and neighbor not in visited:
                    neighbors.append((heuristic(neighbor, end), neighbor))

        neighbors.sort(reverse=True, key=lambda x: x[0])
        for _, neighbor in neighbors:
            stack.append((neighbor, path + [neighbor]))

    return None

def animate_path(path):
    global start
    for pos in path:
        matrix[start[0]][start[1]] = 0
        start = pos
        matrix[start[0]][start[1]] = 2
        update_grid(canvas, matrix)
        canvas.update()
        canvas.after(100)  # Ajusta el tiempo de retraso según sea necesario

def move(event):
    global matrix, start, end, time_left

    if not game_started:
        return

    x, y = start
    if event.keysym == 'Up' and x > 0 and matrix[x-1][y] != 1:
        new_pos = (x-1, y)
    elif event.keysym == 'Down' and x < GRID_SIZE-1 and matrix[x+1][y] != 1:
        new_pos = (x+1, y)
    elif event.keysym == 'Left' and y > 0 and matrix[x][y-1] != 1:
        new_pos = (x, y-1)
    elif event.keysym == 'Right' and y < GRID_SIZE-1 and matrix[x][y+1] != 1:
        new_pos = (x, y+1)
    else:
        new_pos = start

    if new_pos != start:
        matrix[x][y] = 0
        x, y = new_pos
        start = new_pos
        if matrix[x][y] == 3:
            end_game(True)
        matrix[x][y] = 2
        matrix[end[0]][end[1]] = 3
        update_grid(canvas, matrix)

def resolve_maze():
    path = solve_maze(matrix, start, end)
    if path:
        animate_path(path)

def show_info():
    start_screen.pack_forget()
    
    info_text = (
        "Laberinto\n\n"
        "- El objetivo es llegar a la meta (casilla roja) \n desde la casilla inicial (casilla verde).\n\n"
        "- Usa las teclas de dirección para moverte.\n\n"
        "- Tienes un tiempo limitado para resolver el laberinto.\n\n"
        "- Si el tiempo se agota, puedes optar \n por ver la solución del laberinto.\n\n"
        "Creadores:\n"
        "-Patiño Flores Samuel.\n"
        "-Robert Garayzar Arturo.\n"
        "-Sánchez Butrón Eduardo.\n"
    )
    
    info_frame = tk.Frame(root, bg='black')
    info_frame.pack(expand=True, fill='both')
    
    info_label = tk.Label(info_frame, text=info_text, padx=20, pady=50,bg='black', fg='white', font=("Helvetica", 14))
    info_label.pack(expand=True)
    
    back_button = tk.Button(info_frame, text="Regresar al Menú", command=lambda: back_to_menu(info_frame), font=("Helvetica", 16))
    back_button.pack(pady=(0,50))

def back_to_menu(info_frame=None):
    if info_frame:
        info_frame.pack_forget()
    create_start_screen()

def start_game():
    start_screen.pack_forget()
    main_game()

def main_game():
    global canvas, root, timer_label, label_frame, timer_label, game_started, menubar

    menubar = tk.Menu(root)
    root.config(menu=menubar)
    
    options_menu = tk.Menu(menubar, tearoff=0)
    menubar.add_cascade(label="Opciones", menu=options_menu)
    options_menu.add_command(label="Iniciar Juego", command=generate_map)
    options_menu.add_separator()
    options_menu.add_command(label="Salir", command=return_to_menu)

    # Crear etiquetas para indicar el jugador y la meta
    label_frame = tk.Frame(root)
    label_frame.pack(fill=tk.X)
    label_player = tk.Label(label_frame, text="Jugador: Verde", bg="green", fg="white")
    label_player.pack(side=tk.LEFT, expand=True, fill=tk.X)
    label_goal = tk.Label(label_frame, text="Meta: Rojo", bg="red", fg="white")
    label_goal.pack(side=tk.LEFT, expand=True, fill=tk.X)
    timer_label = tk.Label(root, text="Tiempo restante: 0:00", bg="yellow", fg="black")
    timer_label.pack(fill=tk.X)

    canvas = tk.Canvas(root, width=WINDOW_WIDTH, height=GRID_SIZE * CELL_SIZE)
    canvas.pack()

    create_grid(canvas, matrix)

    for direction in ["<Up>", "<Down>", "<Left>", "<Right>"]:
        root.bind(direction, move)

def return_to_menu():
    global game_started, menubar, timer_id
    
    if timer_id:
        canvas.after_cancel(timer_id)
        timer_id = None

    label_frame.pack_forget()
    timer_label.pack_forget()
    canvas.pack_forget()
    menubar.destroy()
    game_started = False
    back_to_menu()

def create_start_screen():
    global start_screen

    start_screen = tk.Frame(root, bg='black')
    start_screen.pack(expand=True, fill='both')

    center_frame = tk.Frame(start_screen, bg='black')
    center_frame.pack(expand=True)

    title_label = tk.Label(center_frame, text="Laberinto", font=("Helvetica", 50), bg="black", fg='white')
    title_label.pack(pady=(0,100))

    start_button = tk.Button(center_frame, text="Jugar", command=start_game, font=("Helvetica", 16), width=23,)
    start_button.pack(pady=10)

    info_button = tk.Button(center_frame, text="Información", command=show_info, font=("Helvetica", 16), width=10)
    info_button.pack(side=tk.LEFT, padx=10)

    exit_button = tk.Button(center_frame, text="Salir", command=root.quit, font=("Helvetica", 16), width=10)
    exit_button.pack(side=tk.RIGHT, padx=10)

def main():
    global root

    root = tk.Tk()
    root.title("Laberinto")
    root.geometry(f"{WINDOW_WIDTH}x{WINDOW_HEIGHT}")

    create_start_screen()

    root.mainloop()

if __name__ == "__main__":
    main()
